import os

dirname = os.path.dirname(__file__)
filename = os.path.join(dirname, '../inputs/day4.txt')
file_content = open(filename, "r")
lines = file_content.readlines()

# Initialize num_of_contained_pairs
num_of_contained_pairs = 0
num_of_overlapped_pairs = 0

# Loop for processing each line
for line in lines:
    first_elf = line.split(",")[0]
    first_elf_start = int(first_elf.split("-")[0])
    first_elf_end = int(first_elf.split("-")[1]) + 1
    first_elf_ids = list(range(first_elf_start, first_elf_end, 1))

    second_elf = line.split(",")[1]
    second_elf_start = int(second_elf.split("-")[0])
    second_elf_end = int(second_elf.split("-")[1]) + 1
    second_elf_ids = list(range(second_elf_start, second_elf_end, 1))

    if len(set(first_elf_ids).intersection(set(second_elf_ids))) > 0:
        num_of_overlapped_pairs +=1

    if set(first_elf_ids).issubset(set(second_elf_ids)):
        num_of_contained_pairs += 1
        continue
    elif set(second_elf_ids).issubset(set(first_elf_ids)):
        num_of_contained_pairs += 1
        continue

print(f"num_of_contained_pairs = {num_of_contained_pairs}")
print(f"num_of_overlapped_pairs = {num_of_overlapped_pairs}")
