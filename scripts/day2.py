# Read from input file and create list of lines
import os

dirname = os.path.dirname(__file__)
filename = os.path.join(dirname, '../inputs/day2.txt')
file_content = open(filename, "r")
lines = file_content.readlines()

# Initialize scores
first_score = 0
second_score = 0

# Function to determine winner
def rock_paper_scissors(opponent_move, player_move):
    if (opponent_move == "ROCK" and player_move == "ROCK"):
        outcome = "DRAW"
    elif (opponent_move == "ROCK" and player_move == "PAPER"):
        outcome = "WIN"
    elif (opponent_move == "ROCK" and player_move == "SCISSORS"):
        outcome = "LOSE"
    elif (opponent_move == "PAPER" and player_move == "ROCK"):
        outcome = "LOSE"
    elif (opponent_move == "PAPER" and player_move == "PAPER"):
        outcome = "DRAW"
    elif (opponent_move == "PAPER" and player_move == "SCISSORS"):
        outcome = "WIN"
    elif (opponent_move == "SCISSORS" and player_move == "ROCK"):
        outcome = "WIN"
    elif (opponent_move == "SCISSORS" and player_move == "PAPER"):
        outcome = "LOSE"
    elif (opponent_move == "SCISSORS" and player_move == "SCISSORS"):
        outcome = "DRAW"

    if outcome == "WIN":
        return 6
    elif outcome == "DRAW":
        return 3
    elif outcome == "LOSE":
        return 0
    else:
        print("This shouldn't run.  What'd you do ?!?")

def compute_my_move(opponent_move, outcome):
    if (opponent_move == "ROCK" and outcome == "WIN"):
        my_move = "PAPER"
    elif (opponent_move == "ROCK" and outcome == "LOSE"):
        my_move = "SCISSORS"
    elif (opponent_move == "ROCK" and outcome == "DRAW"):
        my_move = "ROCK"
    elif (opponent_move == "PAPER" and outcome == "WIN"):
        my_move = "SCISSORS"
    elif (opponent_move == "PAPER" and outcome == "LOSE"):
        my_move = "ROCK"
    elif (opponent_move == "PAPER" and outcome == "DRAW"):
        my_move = "PAPER"
    elif (opponent_move == "SCISSORS" and outcome == "WIN"):
        my_move = "ROCK"
    elif (opponent_move == "SCISSORS" and outcome == "LOSE"):
        my_move = "PAPER"
    elif (opponent_move == "SCISSORS" and outcome == "DRAW"):
        my_move = "SCISSORS"
    return my_move

# Loop through each line of file
for line in lines:
    moves = line.split(" ")
    opp_move_char = moves[0].strip()
    my_move_char = moves[1].strip()

    match opp_move_char:
        case "A":
            opp_move = "ROCK"
        case "B":
            opp_move = "PAPER"
        case "C":
            opp_move = "SCISSORS"

    match my_move_char:
        case "X":
            my_move = "ROCK"
            first_score += 1
        case "Y":
            my_move = "PAPER"
            first_score += 2
        case "Z":
            my_move = "SCISSORS"
            first_score += 3
    first_score += rock_paper_scissors(opponent_move=opp_move, player_move=my_move)

print(f"first_score = {first_score}")

# Second score loop
for line in lines:
    moves = line.split(" ")
    opp_move_char = moves[0].strip()
    outcome_char = moves[1].strip()

    match opp_move_char:
        case "A":
            opp_move = "ROCK"
        case "B":
            opp_move = "PAPER"
        case "C":
            opp_move = "SCISSORS"

    match outcome_char:
        case "X":
            outcome = "LOSE"
            second_score += 0
        case "Y":
            outcome = "DRAW"
            second_score += 3
        case "Z":
            outcome = "WIN"
            second_score += 6

    my_move = compute_my_move(opponent_move=opp_move, outcome=outcome)
    if my_move == "ROCK":
        second_score += 1
    elif my_move == "PAPER":
        second_score += 2
    elif my_move == "SCISSORS":
        second_score += 3

print(f"second_score = {second_score}")