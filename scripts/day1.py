# Read from input file and create list of lines
import os

dirname = os.path.dirname(__file__)
filename = os.path.join(dirname, '../inputs/day1.txt')
file_content = open(filename, "r")
lines = file_content.readlines()

# Set list position for first elf and create empty list to store values
elf_pos = 0
elf_values = []
for line in lines:
    if len(elf_values) != (elf_pos + 1):
        # Initialize the index in the list with a 0
        elf_values.append(0)
    # If the line is not empty, add it to that elf position's value
    if line != "\n":
        new_value = int(line) + int(elf_values[elf_pos])
        elf_values[elf_pos] = new_value
    # If the line is empty, increment to the next elf position
    elif line == "\n":
        elf_pos += 1
    else:
        print("This shouldn't run.  What did you do?!?")

# Create int to sum 3 highest values
top_three = 0

# Loop to find top three
for i in range(0, 3):
    # Get max and add to list
    list_max = max(elf_values)
    if i == 0:
        print(f"The elf with the most has {list_max}")
    top_three += int(list_max)
    # Get index of list_max
    index = elf_values.index(list_max)
    del elf_values[index]

# Print top_three value
print(f"The total of the top three is {top_three}")