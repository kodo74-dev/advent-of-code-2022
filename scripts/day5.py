import os

dirname = os.path.dirname(__file__)
filename = os.path.join(dirname, '../inputs/day5.txt')
file_content = open(filename, "r")
lines = file_content.readlines()

# Initialize lists for each stack
stack1 = []
stack2 = []
stack3 = []
stack4 = []
stack5 = []
stack6 = []
stack7 = []
stack8 = []
stack9 = []

def getNthChars(text, n):
    row_list = []
    for index, character in enumerate(text):
        if (index - 1) % n == 0:
            row_list.append(str(character))
    return row_list

# Initiliaze first loop stopping position
first_loop_stop = 0

# Loop to build initial stack lists
for line in lines:
    first_loop_stop += 1
    row_list = getNthChars(line, 4)

    if row_list == ["1", "2", "3", "4", "5", "6", "7", "8", "9"]:
        break

    # Build each stack backwards
    for idx in range(0, len(row_list), 1):
        vars()['stack' + str(idx + 1)].append(row_list[idx])

for idx in range(0, 9, 1):
    vars()['stack' + str(idx + 1)].reverse()
    vars()['stack' + str(idx + 1)] = [x for x in vars()['stack' + str(idx + 1)] if x.strip()]

multi_stack1 = list(stack1)
multi_stack2 = list(stack2)
multi_stack3 = list(stack3)
multi_stack4 = list(stack4)
multi_stack5 = list(stack5)
multi_stack6 = list(stack6)
multi_stack7 = list(stack7)
multi_stack8 = list(stack8)
multi_stack9 = list(stack9)

# Get list of instructions
instruction_list = lines[(first_loop_stop + 1)::]

# Loop through instruction list
for line in instruction_list:
    line_list = line.split()
    crate_ammount = int(line_list[1])
    source = int(line_list[3])
    destination = int(line_list[5])
    for x in range(0, crate_ammount, 1):
        if len(vars()['stack' + str(source)]) == 0:
            break
        else:
            vars()['stack' + str(destination)].append(vars()['stack' + str(source)][-1])
            vars()['stack' + str(source)].pop()
            continue

print("Single stacks")
print(stack1)
print(stack2)
print(stack3)
print(stack4)
print(stack5)
print(stack6)
print(stack7)
print(stack8)
print(stack9)

# Loop through instruction list for multi stack
for line in instruction_list:
    line_list = line.split()
    crate_ammount = int(line_list[1])
    source = int(line_list[3])
    destination = int(line_list[5])

    vars()['multi_stack' + str(destination)].extend(vars()['multi_stack' + str(source)][-crate_ammount:])

    del eval('multi_stack' + str(source))[-crate_ammount:]


print("Multi stacks")
print(multi_stack1)
print(multi_stack2)
print(multi_stack3)
print(multi_stack4)
print(multi_stack5)
print(multi_stack6)
print(multi_stack7)
print(multi_stack8)
print(multi_stack9)
