import os

dirname = os.path.dirname(__file__)
filename = os.path.join(dirname, '../inputs/day3.txt')
file_content = open(filename, "r")
lines = file_content.readlines()

def find_common_item(line):
    first_half = line[slice(0, len(line)//2)]
    second_half = line[slice(len(line)//2, len(line))]
    common_item = ''.join(set(first_half).intersection(second_half))
    return common_item

item_list = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
             'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

def priority_lookup(common_item):
    item_priority = item_list.index(common_item) + 1
    return item_priority

# Initialize priority value
cumulative_priority = 0

for line in lines:
    common_item = find_common_item(line=line)
    priority = priority_lookup(common_item=common_item)
    cumulative_priority += priority

print(f"cumulative_priority = {cumulative_priority}")

number_of_sets = len(lines)//3
group_cumulative_priority = 0

for idx in range(0, number_of_sets):
    first = lines[(idx*3)].strip()
    second = lines[(idx*3)+1].strip()
    third = lines[(idx*3)+2].strip()
    common_item = ''.join(set(first).intersection(second).intersection(third))
    group_priority = priority_lookup(common_item=common_item)
    group_cumulative_priority += group_priority

print(f"group_cumulative_priority = {group_cumulative_priority}")
